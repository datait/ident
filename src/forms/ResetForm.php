<?php

namespace datait\ident\forms;

use Yii;

class ResetForm extends \yii\base\Model {
	public $email;

	public function rules()	{
		return [
			['email', 'email'],

			['email', 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'email' => Yii::t('ident', 'label.ident.email'),
		];
	}

	public function reset() {
		if (!$this->validate()) {
			return false;
		}

		if ($ident = (Yii::$app->user->identityClass)::findOne(['email' => $this->email])) {
			$ident->createResetEmail();
		}

		return true;
	}
}