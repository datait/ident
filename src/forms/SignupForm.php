<?php

namespace datait\ident\forms;

use common\components\Helper;
use Yii;

class SignupForm extends \yii\base\Model {
	public $email;

	public function rules() {
		return [
			['email', 'email'],
			['email', 'emailValidator'],

			['email', 'required'],
		];
	}

	public function emailValidator($attribute, $params) {
		if ((Yii::$app->user->identityClass)::findOne(['email' => $this->email])) {
			$this->addError($attribute, Yii::t('ident', 'view.signup.error.nonUniqueEmail'));
		}
	}

	public function attributeLabels() {
		return [
			'email' => Yii::t('ident', 'label.ident.email'),
		];
	}

	public function signup() {
		if (!$this->validate()) {
			return false;
		}

		$ident = new Yii::$app->user->identityClass([
			'login' => $this->email,
			'email' => $this->email,
			'role_id' => Yii::$app->getModule('ident')->defaultRoleId,
			'active_id' => (Yii::$app->user->identityClass)::ACTIVE_YES,
			'pass' => (Yii::$app->user->identityClass)::generatePass(),
			'auth_key' => (Yii::$app->user->identityClass)::generateAuthKey(),
		]);

		$ident->save();

		return true;
	}
}
