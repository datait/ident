<?php

namespace datait\ident\forms;

use datait\ident\validators\PassValidator;

class PassChangeForm extends \yii\base\Model
{
	public $current_pass;
	public $pass;
	public $repeat_pass;
	
	public function rules() {
		return array_merge(parent::rules(), [
			['current_pass', PassValidator::class],
			[['pass', 'repeat_pass'], 'string', 'min' => 8, 'message' => Yii::t('ident', 'error.passwordMinimumChars'), 'skipOnEmpty' => false],
			[['repeat_pass', 'pass'], 'filter', 'filter' => 'trim'],
			['repeat_pass', 'compare', 'compareAttribute' => 'pass'],

			[['current_pass', 'pass', 'repeat_pass'], 'required'],
		]);
	}
}