<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;

	$this->title = Yii::t('ident', 'view.reset.title');
?>

<div class="site-login">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="table table-login">
				<div class="table-row">
					<div class="table-cell table-cell-reset">
						<div class="row">
							<div class="col-md-9 col-md-offset-3">
								<h1><?= Html::encode($this->title) ?></h1>

								<p><?= Yii::t('ident', 'view.reset.subtitle') ?></p>
							</div>
						</div>

						<?php $form = ActiveForm::begin([
							'id' => 'login-form',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
								'template' => "{label}<div class='col-md-9'>{input} {error}</div>",
								'labelOptions' => ['class' => 'col-md-3 control-label'],
							],
						]); ?>

						<?= $form->field($model, 'email', ['options' => ['class' => 'form-group form-group-lg']]) ?>

						<div class="row">
							<div class="col-md-9 col-md-offset-3">
								<p class="description"><?= Yii::t('ident', 'view.reset.text.info') ?></p>
							</div>
						</div>

						<div class="form-group bottom">
							<div class="col-lg-offset-3 col-lg-9">
								<?= Html::submitButton(Yii::t('ident', 'view.reset.btn.submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
								<?= Html::a(Yii::t('ident', 'view.reset.btn.back'), ['login'], ['class' => 'btn btn-default']) ?>
							</div>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
