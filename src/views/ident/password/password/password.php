<div class="section">
	<?php $form = \yii\bootstrap\ActiveForm::begin([
		'class' => 'form',
		'layout' => 'horizontal',
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
			'horizontalCssClasses' => [
				'hint' => 'hint',
			],
		],
	]); ?>

		<h3 class="hr"><span>Informacje podstawowe</span></h3>
		<div class="form-block">
			<p>Od ostatniej zmiany hasła upłynęło już <?= \Yii::$app->params['areas']['user']['passwordChangeDays'] / (60 * 60 * 24) ?> dni i konieczna jest zmiana hasła.</p>
			<?= $form->field($model, 'fld_password', ['options' => ['class' => 'form-group form-group-lg']])->passwordInput()->label('Nowe hasło') ?>
			<?= $form->field($model, 'frm_password_repeat', ['options' => ['class' => 'form-group form-group-lg']])->passwordInput() ?>
		</div>

		<div class="row">
			<div class="col-md-9 col-md-offset-3">
				<div class="bottom">
					<?= \yii\helpers\Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
				</div>
			</div>
		</div>

	<?php \yii\bootstrap\ActiveForm::end() ?>
</div>