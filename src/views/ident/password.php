<?php
	use app\models\User;
?>

<?= \app\widgets\Breadcrumbs::widget() ?>
<h1>Zmiana hasła</h1>

<?php
	$items[] = [
		'label' => \Yii::$app->user->identity->role['name'],
		'content' => $this->render('@app/modules/entity/views/entity/view/view', ['model' => $model]),
	];

	$items[] = [
		'label' => 'Zmiana hasła',
		'content' => $this->render('password/password', ['model' => $model]),
		'active' => true,
		'headerOptions' => ['class' => 'main'],
	];

	echo \app\components\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>