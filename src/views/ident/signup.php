<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;

	$this->title = Yii::t('ident', 'view.signup.title');
?>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="brand-header">
			<a href="<?= Yii::$app->request->hostName ?>"><img src="/img/logo.png" /></a>
		</div>
		<div class="table table-login">
			<div class="table-row">
				<div class="table-cell table-cell-login-brand hidden-xs">
					<img src="img/login.png" style="width: 70%" />
				</div>
				<div class="table-cell table-cell-login">
					<div class="site-signup">
						<h1><?= Html::encode($this->title) ?></h1>

						<p><?= Yii::t('ident', 'view.signup.subtitle') ?></p>

						<?php $form = ActiveForm::begin([
							'id' => 'login-form',
							'layout' => 'horizontal',
							'fieldConfig' => [
							'template' => '<div class="col-sm-12">{input}{hint}{error}</div>',
							'horizontalCssClasses' => [
								'hint' => 'hint',
							],
						],
							'enableClientValidation' => false,
						]); ?>

							<?= $form->field($model, 'email', ['options' => ['class' => 'form-group form-group-lg']])->textInput(['placeholder' => Yii::t('ident', 'label.ident.email')]) ?>

							<p class="description"><?= Yii::t('ident', 'view.signup.text.info') ?></p>

							<div class="form-group bottom">
								<div class="col-md-9">
									<div class="btn-group">
										<?= Html::submitButton(Yii::t('ident', 'view.signup.btn.submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
									</div>
									<div class="btn-group">
										<?= Html::a(Yii::t('ident', 'view.signup.btn.back'), ['login'], ['class' => 'btn btn-default']) ?>
									</div>
								</div>
							</div>

						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="undertab-content">
			<div class="ip"><?= $_SERVER['SERVER_ADDR'] ?></div>
			<div class="datait-brand">created by <a href="http://datait.pl" target="_blank">datait.pl</a></div>
		</div>

		<div class="policy-content">
			<p><?= Yii::t('ident', 'view.login.text.policy') ?></p>
		</div>
	</div>
</div>