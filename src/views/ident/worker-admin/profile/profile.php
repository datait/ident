<?php
	use yii\helpers\Html;
?>

<div class="section">
	<div class="form-horizontal">
		<h3 class="hr"><span>Informacje podstawowe</span></h3>
		<div class="form-group form-group-lg">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_full_name') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::a(Html::encode($model->fld_name), ['worker/view', 'id' => $model->fld_id]) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_pesel') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?php if ($model->fld_no_pesel_id): ?>
						<span class="label label-<?= $model->noPesel['class'] ?>" disabled="disabled">Brak numeru pesel</span>
					<?php else: ?>
						<?= Html::encode($model->fld_pesel) ?>
					<?php endif ?>
				</div>
			</div>
		</div>

		<?php if (\Yii::$app->params['areas']['unit']['enabled']): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_unit_id') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= Html::a(Html::encode($model->unit->fld_name), ['unit/view', 'id' => $model->fld_unit_id]) ?>
					</div>
				</div>
			</div>
		<?php endif ?>

		<?php if (\Yii::$app->user->can('//worker/worker/getActive', ['model' => $model])): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_active_id') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<span class="label label-<?= $model->active['class'] ?>" disabled="disabled"><?= $model->active['name'] ?></span>
					</div>
				</div>
			</div>
		<?php endif ?>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_role_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->role['class'] ?>"><?= $model->role['name'] ?></span>
				</div>
			</div>
		</div>

		<?php if ($model->fld_sub_id): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_sub_id') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= $model->sub['name'] ?>
					</div>
				</div>
			</div>
		<?php endif ?>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_email') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::a(Html::encode($model->fld_email), 'mailto:' . $model->fld_email) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_mobile') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_mobile) ?>
				</div>
			</div>
		</div>

		<h3 class="hr"><span>Dane do logowania</span></h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_login') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_login) ?>
				</div>
			</div>
		</div>

		<h3 class="hr"><span>Adres zameldowania</span></h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_street_registered') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_street_registered) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_code_registered') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_code_registered) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_city_registered') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_city_registered) ?>
				</div>
			</div>
		</div>

		<h3 class="hr"><span>Adres kontaktowy</span></h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_street_contact') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_street_contact) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_code_contact') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_code_contact) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_city_contact') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_city_contact) ?>
				</div>
			</div>
		</div>

		<h3 class="hr"><span>Informacje dodatkowe</span></h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_description') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= \yii\helpers\Html::encode($model->fld_description) ?>
				</div>
			</div>
		</div>
	</div>
</div>
