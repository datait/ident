<?php
	use yii\helpers\Html;
?>

<div class="section search">
	<?php if (\Yii::$app->user->can('//user/user/password', ['model' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Zmień hasło', ['/password'], ['class' => 'btn btn-warning']) ?>
		</div>
	<?php endif ?>
</div>

<?= $this->render('view/view', ['model' => $model]) ?>

<?= $this->render('@log/views/log/footer', ['model' => $model]) ?>