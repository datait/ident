<?php

use app\models\User;

?>

<?php if (\Yii::$app->user->identity->fld_role_id == User::ROLE_ENTITY_PERSON): ?>
	<?= $this->render($model->fld_role_id . '/profile', ['model' => $model]) ?>
<?php else: ?>
	<?= \app\widgets\Breadcrumbs::widget() ?>
	<h1>Profil</h1>

	<?php
		$items[] = [
			'label' => 'Pracownik',
			'content' => $this->render($model->fld_role_id . '/profile', ['model' => $model]),
			'active' => true,
			'headerOptions' => ['class' => 'main'],
		];

		echo \app\components\Tabs::widget([
			'items' => $items,
			'encodeLabels' => false,
		]);
	?>
<?php endif ?>