<?php
	use yii\helpers\Html;
	use app\libs\Helper;

	use app\modules\target\models\Subscription;
    use app\modules\target\search\SubscriptionSearch;

	$searchModel = new SubscriptionSearch;
	$searchModel->view = ['entity/view', 'id' => $model->fld_id, 'tab' => 'subscriptions'];

	$searchModel->entity_id = $model->fld_id;

	$dataProvider = $searchModel->search();

	$dataProvider->pagination->pageParam = 'subscriptions';
	$dataProvider->pagination->params = array_merge($_GET, ['tab' => 'subscriptions']);
?>

<?php $form = \yii\bootstrap\ActiveForm::begin([
		'layout' => 'horizontal',
		'options' => [
			'class' => 'form form-entity-person-update',

			'enctype' => 'multipart/form-data',
			'style' => 'margin-bottom: 15px'
		],
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
			'horizontalCssClasses' => [
				'hint' => 'hint',
			],
		],
	]); ?>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<a href="<?= \Yii::$app->params['protocol'] . '://' . \Yii::$app->params['domain'] ?>"><img src="/img/<?= SYSTEM ?>/<?= \Yii::$app->params['logo']['login'] ?>" /></a>
		</div>
	</div>

	<div class="row" style="margin-top: 50px">
		<div class="col-md-8 col-md-offset-2">
			<?php if ($lang_id == 'pl'): ?>
				<p>Wychodząc naprzeciw Państwa oczekiwaniom, zespół prawników SDZLEGAL SCHINDHELM przygotowuje prawniczy newsletter, w którym dostarcza Państwu informacji o najistotniejszych zmianach w prawie, nadchodzących regulacjach i kluczowych orzeczeniach sądowych. Na bieżąco informujemy o najważniejszych zagadnieniach prawnych i o wydarzeniach z życia Kancelarii.</p>
				<h3 style="text-align: center">FORMULARZ REJESTRACYJNY</h3>
				<p style="font-weight: 600">Wyrażam zgodę na:</p>
			<?php elseif ($lang_id == 'en'): ?>
				<p>In order to meet your expectations, the team of SDZLEGAL SCHINDHELM prepares a legal Newsletter, which provides you with information about the most important changes in law, upcoming regulations and key court judgments. We keep you up to date on the most important legal issues and we inform you about events organized by our Law Office.</p>
				<h3 style="text-align: center">REGISTRATION FORM</h3>
				<p style="font-weight: 600">I hereby grant my consent to:</p>
			<?php elseif ($lang_id == 'de'): ?>
				<p>Um Ihre Erwartungen zu erfüllen, bereitet das Team von SDZLEGAL SCHINDHELM einen juristischen Newsletter vor, in dem wir über wichtigste Rechtsänderungen, kommende Regelungen und wesentliche Gerichtsentscheidungen informieren möchten. Wir halten Sie über die wichtigsten juristischen Themen und Veranstaltungen der Kanzlei auf dem Laufenden.</p>
				<h3 style="text-align: center">REGISTRIERUNGSFORMULAR</h3>
				<p style="font-weight: 600">Ich willige ein, dass:</p>
			<?php endif ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="section">
				<?= \yii\grid\GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
					'showHeader' => false,
					'tableOptions' => [
						'class' => 'table table-hover',
					],
					'pager' => false,
					'summary' => false,
					'options' => [
						'style' => 'margin-left: 0; margin-right: 0',
					],
					'columns' => [
						[
							'value' => function ($data) {
								$html = [];
								if ($data->target->fld_resignation_id == \app\modules\target\models\Target::RESIGNATION_NO) {
									$html = ['disabled' => 'disabled'];
								}

								return Html::checkbox('agreements[' . $data->fld_id . ']', $data->fld_active_id, $html);
							},
							'format' => 'raw',
							'contentOptions' => [
								'style' => 'width: 40px; text-align: right',
							],
						],
						[
							'label' => 'Cel przetwarzania',
							'value' => function($data) use ($lang_id) {
								switch ($lang_id) {
									case 'en':
										$main = $data->target->fld_description_en;

										break;

									case 'de':
										$main = $data->target->fld_description_de;

										break;

									default:
										$main = $data->target->fld_description_pl;
								}

								return $main;
							},
							'format' => 'html',
							'headerOptions' => [
								'class' => 'col-main'
							],
							'contentOptions' => [
								'class' => 'col-main',
								'style' => 'padding-left: 0',
							],
						],
					],
				]); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<?php if ($lang_id == 'pl'): ?>
				<p style="font-weight: 600">Zostałem poinformowany o tym, że:</p>
				<ol>
					<li>Administratorem danych jest Kancelaria Prawna Schampera, Dubis, Zając i Wspólnicy sp. k. z siedzibą we Wrocławiu (50-077) przy ul. Kazimierza Wielkiego 3, wpisana do Rejestru Przedsiębiorców Krajowego Rejestru Sądowego przez Sąd Rejonowy dla Wrocławia-Fabrycznej we Wrocławiu, VI Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000131331, NIP: 8951780757, REGON: 932832704 (dalej "<span style="font-weignt: 600">Kancelaria</span>").</li>
					<li>Dane osobowe (imię i nazwisko oraz adres email) będą przetwarzane wyłącznie na potrzeby przesyłania Newslettera oraz informacji nt. szkoleń, konferencji i innych wydarzeń organizowanych przez Kancelarię. Moje dane będą wykorzystywane w celu wysłania tych informacji na podstawie ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną oraz zgodnie z rozporządzeniem ogólnym o ochronie danych osobowych (RODO).</li>
					<li>Podanie danych w zakresie imienia i nazwiska oraz adresu e-mail jest dobrowolne, ale niezbędne dla otrzymania Newslettera oraz informacji nt. szkoleń, konferencji i innych wydarzeń organizowanych przez Kancelarię. Niepodanie tych danych uniemożliwi przesłanie Newslettera i informacji nt. szkoleń, konferencji i innych wydarzeń organizowanych przez Kancelarię.</li>
					<li>Dane w zakresie imienia i nazwiska, adresu e-mail będą przetwarzane przez czas korzystania z Newslettera lub przekazywania informacji o szkoleniach, konferencjach i innych wydarzeniach, ewentualnie do czasu przedawnienia roszczeń mogących powstawać w powyższym zakresie.</li>
					<li>Przetwarzanie podanych danych następuje w oparciu o dobrowolną zgodę (art. 6 ust. 1 lit. a) RODO). Dane będą mogły być również przetwarzane w celu dochodzenia lub obrony roszczeń, jako uzasadniony interes administratora danych (art. 6 ust. 1 lit. f) RODO).</li>
					<li>dane osobowe mogą być przekazywane podmiotom współpracującym z Kancelarią, w szczególności kancelariom w ramach sieci Schindhelm, podmiotom świadczącym usługi IT oraz usługi archiwizacji.</li>
					<li>Dane osobowe mogą być przekazywane również do państw trzecich, tj. poza Unię Europejską. W takim przypadku Kancelaria dba o poufność przekazywanych danych oraz o to, aby przekazywać wyłącznie dane niezbędne do realizacji celu oraz w oparciu o odpowiednie zabezpieczenia zgodne z przepisami o ochronie danych osobowych.</li>
					<li>W zakresie przewidzianym prawem przysługuje mi prawo do żądania dostępu do danych osobowych, ich sprostowania, usunięcia lub ograniczenia przetwarzania, a także prawo do wniesienia sprzeciwu wobec przetwarzania oraz prawo do przenoszenia danych. Przysługuje mi także prawo do wniesienia skargi do organu nadzorczego (Prezes Urzędu Ochrony Danych Osobowych).</li>
					<li style="font-weight: 600">Przysługuje mi  prawo do cofnięcia zgody w dowolnym momencie bez wpływu na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej cofnięciem.</li>
				</ol>
			<?php elseif ($lang_id == 'en'): ?>
				<p style="font-weight: 600">I was informed about the fact that:</p>
				<ol>
					<li>The Controller of data is Kancelaria Prawna Schampera, Dubis, Zając i Wspólnicy sp. k. with its registered seat in Wrocław (50-077), ul. Kazimierza Wielkiego 3, entered in the Register of Entrepreneurs of the National Court Register by the District Court for Wrocław-Fabryczna in Wrocław, 6th  Commercial Department of the National Court Register under KRS number 0000131331, NIP: 8951780757, REGON: 932832704 (hereinafter referred to as: "<span style="font-weight: 600">Law Office</span>").</li>
					<li>Personal data (name and surname, e-mail address) will be processed only for the purposes of sending the Newsletter and information on training, conferences and other events organized by the Law Office. My data will be used to send this information based on the Act of 18 July 2002 on the provision of electronic services and in accordance with the General Regulation on Personal Data Protection (GDPR).</li>
					<li>Providing data (name and surname, e-mail address) is voluntary, however necessary to receive the Newsletter and information on training, conferences and other events organized by the Law Office. Failure to provide such data will prevent us from sending the Newsletter and information on trainings, conferences and other events organized by the Law Office.</li>
					<li>Personal data (name and surname, e-mail address) will be processed for a period of subscription of the Newsletter or sending information on training, conferences and other events, possibly until the expiry of limitation period for claims that may arise in the above-mentioned scope.</li>
					<li>Data processing is based on the consent freely given (Article 6 (1) (a) GDPR). The data can also be processed in connection with the investigation / defense of claims as the legitimate interest of the Controller of data (Article 6 (1) (f) GDPR).</li>
					<li>my personal data may be transferred to the entities cooperating with the Law Office, in particular to the law offices within the Schindhelm network, entities providing IT services and archiving services.</li>
					<li>My personal data may also be transferred to third countries, ie. outside the European Union. In this case, the Law Office takes care of the confidentiality of the data transmitted and transfers only data that are necessary to perform tasks in object to achieve the objectives and based on appropriate safeguards in accordance with the data protection law.</li>
					<li>To the extent provided by the provisions of law I have the right to request access to my personal data, rectification or erasure of personal data or restriction of processing as well as the right to object to processing and the right to data portability. I also have the right  to lodge a complaint with a competent supervisory authority, i.e. the President of the Office for the Protection of Personal Data (Prezes Urzędu Ochrony Danych Osobowych).</li>
					<li style="font-weight: 600">I have also the right to withdraw my consent at any time, without affecting the lawfulness of the processing of data which was carried out on the basis of the consent prior to its withdrawal.</li>
				</ol>
			<?php elseif ($lang_id == 'de'): ?>
				<p style="font-weight: 600">Ich wurde darüber informiert, dass:</p>
				<ol>
					<li>die Kanzlei Kancelaria Prawna Schampera, Dubis, Zając i Wspólnicy sp. k. mit Sitz in Wrocław (50-077) ul. Kazimierza Wielkiego 3, eingetragen im Unternehmerregister des Landesgerichtsregisters durch das Amtsgericht für Wrocław-Fabryczna in Wrocław, 6. Wirtschaftsabteilung des Landesgerichtsregisters, unter der KRS-Nummer 0000131331, NIP-Nummer: 8951780757, REGON-Nummer: 932832704 (weiter "<span style="font-weight: 600">Kanzlei</span>" genannt) der Verantwortliche meiner personenbezogenen Daten ist.</li>
					<li>die personenbezogenen Daten (Name und Vorname sowie E-Mail-Adresse) ausschließlich zu den Zwecken des Versands des Newsletters und der Informationen über durch die Kanzlei veranstaltete Schulungen, Konferenzen und andere Events verarbeitet werden. Meine Daten werden zu den Zwecken des Versands von diesen Informationen aufgrund des Gesetzes vom 18. Juli 2002 über elektronisch erbrachte Dienstleistungen und gemäß Datenschutz-Grundverordnung (DSGVO) verarbeitet.</li>
					<li>die Bereitstellung meines Namens, Vornamens und der E-Mail-Adresse freiwillig, jedoch für den Versand des Newsletters und der Informationen über Schulungen, Konferenzen und andere Events erforderlich ist. Die Nichtbereitstellung macht den Versand des Newsletters und der Informationen über Schulungen, Konferenzen und andere Events der Kanzlei unmöglich.</li>
					<li>die Daten über Name und Vorname sowie E-Mail-Adresse nicht länger als während der Dauer des Abonnierens des Newsletters oder Versands der Informationen über Schulungen, Konferenzen und andere Events verarbeitet werden. Meine Daten können auch während der Verjährungsfrist der mit der Datenverarbeitung entstandenen Ansprüche verarbeitet werden.</li>
					<li>die Daten aufgrund meiner freiwilligen Einwilligung (Art. 6 Abs. 1 Buchst. a) der DSGVO) verarbeitet werden. Die Daten können auch zum Zweck der Geltendmachung oder Verteidigung von Ansprüchen als berechtigte Interessen des Verantwortlichen (Art. 6 Abs. 1 Buchst. f) der DSGVO) verarbeitet werden.</li>
					<li>meine personenbezogenen Daten an die mit der Kanzlei zusammenarbeitenden Unternehmen, u.a. die Kanzleien im Rahmen des Schindhelm-Netzwerkes sowie an Unternehmen, die IT- und Archievierungsdienstleistungen erbringen, übermittelt werden können.</li>
					<li>meine personenbezogenen Daten den Drittländern, d.h. außerhalb der Europäischen Union, übermittelt werden können. In diesem Fall sorgt die Kanzlei für die Vertraulichkeit der übermittelten Daten sowie dafür, dass ausschließlich Daten übermittelt werden, die zur Erreichung des Ziels notwendig sind und die erforderlichen Sicherungen gemäß den datenschutzrechtlichen Vorschriften eingehalten werden.</li>
					<li>in dem durch die Rechtsvorschriften vorgesehenen Umfang mir das Recht auf Auskunft über meine personenbezogenen Daten, auf Berichtigung, Löschung oder auf Einschränkung der Verarbeitung, oder das Widerspruchsrecht gegen die Verarbeitung sowie das Recht auf Datenübertragbarkeit zu steht. Mir steht auch das Beschwerderecht bei der Aufsichtsbehörde, d.h. Prezes Urzędu Ochrony Danych Osobowych (Präsident der Datenschutzbehörde) zu.</li>
					<li style="font-weight: 600">mir das Recht zu steht, die Einwilligung jederzeit zu widerrufen. Der Widerruf der Einwilligung berührt nicht die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung.</li>
				</ol>
			<?php endif ?>

			<p style="font-weight: 600; margin-top: 25px">SDZLEGAL Schindhelm 
			<br>Kancelaria Prawna Schampera, Dubis, Zając i Wspólnicy sp. k. 
			<br>ul. Kazimierza Wielkiego 3, 50-077 Wrocław 
			<br>E-Mail: wroclaw@sdzlegal.pl 
			<br><a href="http://www.schindhelm.com">www.schindhelm.com</a></p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="bottom">
				<?php
					if ($lang_id == 'pl') {
						$save = 'Zapisz';
					} elseif ($lang_id == 'en') {
						$save = 'Save';
					} elseif ($lang_id == 'de') {
						$save = 'Speichern';
					}
				?>
				<?= \yii\helpers\Html::submitButton($save, ['class' => 'btn btn-success']) ?>
			</div>
		</div>
	</div>

<?php \yii\bootstrap\ActiveForm::end() ?>