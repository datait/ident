<?php
	use yii\helpers\Html;
	use app\libs\Helper;

	use app\modules\target\models\Subscription;
    use app\modules\target\search\SubscriptionSearch;

	$searchModel = new SubscriptionSearch;
	$searchModel->view = ['entity/view', 'id' => $model->fld_id, 'tab' => 'subscriptions'];

	$searchModel->entity_id = $model->fld_id;

	$dataProvider = $searchModel->search();

	$dataProvider->pagination->pageParam = 'subscriptions';
	$dataProvider->pagination->params = array_merge($_GET, ['tab' => 'subscriptions']);
?>

<?php $form = \yii\bootstrap\ActiveForm::begin([
		'layout' => 'horizontal',
		'options' => [
			'class' => 'form form-entity-person-update',

			'enctype' => 'multipart/form-data',
			'style' => 'margin-bottom: 15px'
		],
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
			'horizontalCssClasses' => [
				'hint' => 'hint',
			],
		],
	]); ?>
	<div class="row" style="margin-top: 40px">
		<div class="col-md-8 col-md-offset-2" style="text-align: center">
			<a href="<?= \Yii::$app->params['protocol'] . '://' . \Yii::$app->params['domain'] ?>"><img src="/img/<?= SYSTEM ?>/<?= \Yii::$app->params['logo']['login'] ?>" /></a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<p style="text-align: center; font-weight: 600; margin-top: 12px">Joy of Ideas</p>
			<h3 style="text-align: center">PODSTAWOWE INFORMACJE O RODO</h4>
			<p>Zależy mi na Twoim zaufaniu i naszej reputacji. Dlatego dane osobowe przetwarzam w minimalnym zakresie.</p>
			<h4 style="text-align: center">UAKTUALNIJ SWOJE ZGODY</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="section">
				<?= \yii\grid\GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
					'showHeader' => false,
					'tableOptions' => [
						'class' => 'table table-hover',
					],
					'pager' => false,
					'summary' => false,
					'options' => [
						'style' => 'margin-left: 0; margin-right: 0',
					],
					'columns' => [
						[
							'value' => function ($data) {
								$html = [];
								if ($data->target->fld_resignation_id == \app\modules\target\models\Target::RESIGNATION_NO) {
									$html = ['disabled' => 'disabled'];
								}

								return Html::checkbox('agreements[' . $data->fld_id . ']', $data->fld_active_id, $html);
							},
							'format' => 'raw',
							'contentOptions' => [
								'style' => 'width: 40px; text-align: right',
							],
						],
						[
							'label' => 'Cel przetwarzania',
							'value' => function($data) {
								return $main = '<div style="margin-top: 2px"><strong>' . $data->target->fld_name . '</strong><br>' . $data->target->fld_description_pl . '</div>';
							},
							'format' => 'html',
							'headerOptions' => [
								'class' => 'col-main'
							],
							'contentOptions' => [
								'class' => 'col-main',
								'style' => 'padding-left: 0',
							],
						],
					],
				]); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h4 style="text-align: center">ADMINISTRATOR</h4>
			<p>Administratorem Twoich danych osobowych jest firma Anna Sapieha Communication (zwana dalej ASC) z siedzibą w Łodzi, ul. Gołębia 2 l.40, 90-340  o numerze REGON: 362193547, NIP 7292479934.</p>
			<h4 style="text-align: center">PO CO PRZETWARZAM DANE?</h4>
			<p>Twoje dane mogą być wykorzystane w celach:</p>
			<ul>
				<li>Media Relations - dostarczanie drogą e-mailową/telefoniczną informacji prasowych i atrakcyjnego contentu (Twoja dobrowolna zgoda)</li>
				<li>wysyłki zapytań ofertowych i dalszej korespondencji o charakterze biznesowym i handlowym (Twoja dobrowolna zgoda)</li>
				<li>zawarcia i realizacji łączącej nas umowy (wykonanie umowy) lub umów zawartych przed 25.05.2018 r.</li>
				<li>ustalenia, obrony oraz dochodzenia roszczeń (prawnie uzasadniony interes)</li>
				<li>marketingu bezpośredniego (prawnie uzasadniony interes)</li>
				<li>promocji własnych usług (prawnie uzasadniony interes)</li>
				<li>tworzenia zestawień, analizy i statystyk (uzasadniony interes)</li>
				<li>rozliczeń finansowych; wystawiania dokumentów księgowych (obowiązek prawny, podatkowy)</li>
			</ul>
			<h4 style="text-align: center">KOMU POWIERZAM PRZETWARZANIE TWOICH DANYCH?</h4>
			<p>Powierzam przetwarzanie Twoich danych osobowych innym firmom; partnerom, podwykonawcom tylko wtedy kiedy jest to niezbędne do realizacji naszej umowy lub wynika to z obowiązku prawnego albo uzasadnionego interesu;</p>
			<ul>
				<li>agencjom reklamowym, podmiotom współpracującym przy działaniach PR i marketingowych</li>
				<li>kurierom, pracownikom poczty</li>
				<li>bankom, instytucjom płatniczym</li>
				<li>podmiotom współpracującym ze mną przy obsłudze księgowej, prawnej, IT</li>
				<li>firmom windykacyjnym</li>
			</ul>
			<p>Twoje dane mogą trafić poza Europejski Obszar Gospodarczy jeśli moim partnerem lub podwykonawcą w łączącej nas umowie jest firma działająca na terenie kraju spoza EOG</p>
			<h4 style="text-align: center">JAK DŁUGO PRZETWARZAM DANE?</h4>
			<ul>
				<li>nie dłużej niż jest to konieczne do realizacji umowy</li>
				<li>nie dłużej niż do momentu wycofania przez Ciebie zgody</li>
				<li>nie dłużej niż wynika z przepisów prawa, w tym podatkowego</li>
			</ul>
			<h4 style="text-align: center">DO CZEGO MASZ PRAWO?</h4>
			<ul>
				<li>dostępu do swoich danych</li>
				<li>żądania ich sprostowania, usunięcia lub ograniczenia</li>
				<li>wycofania zgody</li>
				<li>żądania kopii swoich danych i ich przenoszenia</li>
				<li>wniesienia skargi do organu nadzorczego od ochrony danych osobowych</li>
			</ul>
			<h4 style="text-align: center">CHCESZ WIEDZIEĆ WIĘCEJ?</h4>
			<p>Zapytaj, upewnij się <a href="mailto:anna@sapieha.com.pl">anna@sapieha.com.pl</a> kom: 510 022 523</p>
			<p>*Treść klauzuli informacyjnej zgodnie z art. 13 ust. 1 Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 roku w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (zwanej RODO)</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="bottom" style="text-align: center">
				<?= \yii\helpers\Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
			</div>
		</div>
	</div>

<?php \yii\bootstrap\ActiveForm::end() ?>