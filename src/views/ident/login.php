<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('ident', 'view.login.title');

?>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="brand-header">
			<a href="<?= Yii::$app->request->hostName ?>"><img src="/img/logo.png" /></a>
		</div>
		<div class="table table-login">
			<div class="table-row">
				<div class="table-cell table-cell-login-brand hidden-xs">
					<img src="img/login.png" style="width: 70%" />
				</div>
				<div class="table-cell table-cell-login">
					<div class="site-login">
						<h1><?= Html::encode($this->title) ?></h1>

						<p><?= Yii::t('ident', 'view.login.subtitle') ?></p>

						<?php $form = ActiveForm::begin([
							'id' => 'login-form',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
								'template' => "<div class='col-md-12'>{input} {error}</div>",
							],
							'enableClientValidation' => false,
						]); ?>

						<?= $form->field($model, 'login', ['options' => ['class' => 'form-group form-group-lg']])->textInput(['placeholder' => Yii::t('ident', 'label.ident.email')]) ?>
						<?= $form->field($model, 'pass')->passwordInput(['placeholder' => Yii::t('ident', 'label.ident.pass')]) ?>

						<div class="form-group bottom">
							<div class="col-md-12">
								<div class="btn-group">
									<?= Html::submitButton(Yii::t('ident', 'view.login.btn.submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
								</div>
								<div class="btn-group">
									<?= Html::a(Yii::t('ident', 'view.login.btn.passForgotten'), ['reset'], ['class' => 'btn btn-default']) ?>
								</div>
							</div>
							<div class="col-md-12" style="margin-top: 15px">
								<?= Html::a(Yii::t('ident', 'view.login.btn.newAccount'), ['signup']) ?>
							</div>
						</div>

						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="undertab-content">
			<div class="ip"><?= $_SERVER['SERVER_ADDR'] ?></div>
			<div class="datait-brand">created by <a href="http://datait.pl" target="_blank">datait.pl</a></div>
		</div>

		<div class="policy-content">
			<p><?= Yii::t('ident', 'view.login.text.policy') ?></p>
		</div>
	</div>
</div>
