<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ident`.
 */
class m180824_162110_create_ident_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ident}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'pass' => $this->string()->notNull(),
            'pass_reset_token' => $this->string()->unique(),
            'name' => $this->string(),
            'email' => $this->string(),
            'role_id' => $this->string()->notNull(),
            'sub_id' => $this->string(),
            'active_id' => $this->smallInteger()->notNull(),

            'creator_id' => $this->integer(),
            'created_at' => $this->bigInteger()->notNull(),
            'updater_id' => $this->integer(),
            'updated_at' => $this->bigInteger(),
            'deleter_id' => $this->integer(),
            'deleted_at' => $this->bigInteger(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%ident}}');
    }
}
