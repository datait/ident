<?php

namespace datait\ident\validators;

use Yii;

class PassValidator extends \yii\validators\Validator
{
	public function validateAttribute($model, $attribute)
	{
		if (!Yii::$app->security->validatePassword($model->$attribute, $model->pass)) {
			$this->addError($model, $attribute, Yii::t('ident', 'error.wrongCurrentPass'));
		}
	}
}