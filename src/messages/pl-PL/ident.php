<?php

return [
	'ident' => 'Konto',

	'label.ident.email' => 'E-mail',
	'label.ident.pass' => 'Hasło',
	'label.ident.name' => 'Nazwa firmy lub imię i nazwisko',

	'view.login.title' => 'Logowanie',
	'view.login.subtitle' => 'Wprowadź e-mail i hasło',
	'view.login.text.policy' => 'Użytkownicy są zobowiązani do zabezpieczenia danych służących do uzyskania dostępu do systemu przed osobami niepowołanymi. Próby nieautoryzowanego dostępu do systemu są zabronione i traktowane będą jako naruszenie obowiązujących norm prawnych.',
	'view.login.btn.submit' => 'Zaloguj',
	'view.login.btn.passForgotten' => 'Resetuj hasło',
	'view.login.btn.newAccount' => 'Utwórz nowe konto',

	'view.login.error.wrongLoginOrPass' => 'Błędny login lub hasło',

	'view.signup.title' => 'Tworzenie nowego konta',
	'view.signup.subtitle' => 'Wprowadź nazwę i e-mail',
	'view.signup.text.info' => 'Po utworzeniu konta otrzymasz e-mail z linkiem do logowania, po zalogowaniu będziesz mógł utworzyć hasło.',
	'view.signup.btn.submit' => 'Utwórz konto',
	'view.signup.btn.back' => 'Wróć',
	
	'view.signup.error.incorrectRole' => 'Niepoprawna rola',
	'view.signup.error.incorrectType' => 'Niepoprawny typ',
	'view.signup.error.nonUniqueEmail' => 'Ten e-mail został już użyty',

	'view.reset.title' => 'Reset hasła',
	'view.reset.subtitle' => 'Wprowadź e-mail w celu resetu hasła',
	'view.reset.btn.submit' => 'Resetuj hasło',
	'view.reset.btn.back' => 'Wróć',
	'view.reset.text.info' => 'Jeżeli podany e-mail znajduje się w naszej bazie, otrzymasz wiadomość e-mail z linkiem do formularza ustalania nowego hasła.',

	'view.reset.error.incorrectEmail' => 'Niepoprawny e-mail',
];