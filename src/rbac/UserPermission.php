<?php

namespace app\modules\user\rbac;

use app\models\User;

class UserPermission extends \app\rbac\Permission {
	public static function password($params = []) {
		return true;
	}

	public static function profile($params = []) {
		if ($params['user']->isInRole('entity-person') && $params['model']->id == \Yii::$app->user->identity->fld_id) {
			return true;
		} elseif ($params['user']->isInRole(['worker-admin', 'manager'])) {
			return true;
		}
	}

	public static function view($params = []) {
		return true;
	}
}
