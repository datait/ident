<?php

namespace datait\ident\controllers;

use datait\ident\forms\LoginForm;
use datait\ident\forms\SignupForm;
use datait\ident\forms\ResetForm;
use datait\ident\models\Ident;
use Yii;
use yii\web\NotFoundHttpException;

class IdentController extends \yii\web\Controller {
	public function behaviors() {
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['login', 'logout', 'pass', 'profile'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['login', 'signup', 'reset'],
						'roles' => ['?'],
					],
				],
			],
		];
	}

	public function beforeAction($action) {
		if (!parent::beforeAction($action)) {
			return false;
		}

		if (in_array($action->id, ['profile', 'login', 'signup', 'reset'])) {
			$this->layout = '/guest';
		}

		return true;
	}
    
    public function actionLogin($key = null, $lang_id = null) {
		Yii::$app->user->logout();

		if (is_null($key)) {
			$model = new LoginForm();
			if ($model->load(\Yii::$app->request->post()) && $model->login()) {
				return $this->goHome();
			} else {
				return $this->render('login', ['model' => $model]);
			}
		} else {
			if ($model = (Yii::$app->user->identityClass)::findOne(['key_id' => $key, 'active_id' => Ident::ACTIVE_YES])) {
				$model->confirm();

				Yii::$app->user->login($model);

				if (Yii::$app->user->identity->isInRole('entityPerson')) {
					return $this->redirect(['//ident/ident/profile', 'lang_id' => $lang_id]);
				} elseif (\Yii::$app->user->identity->isInRole('workerAdmin')) {
					return $this->redirect(['//ident/ident/index']);
				} else {
					return $this->goHome();
				}
			} else {
				throw new NotFoundHttpException('Brak uprawnień');
			}
		}
	}

	public function actionSignup() {
		Yii::$app->user->logout();

		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post()) && $model->signup()) {
			return $this->goHome();
		}
		
		return $this->render('signup', ['model' => $model]);
	}
    
    public function actionReset() {
		Yii::$app->user->logout();

		$model = new ResetForm();
		if ($model->load(Yii::$app->request->post()) && $model->reset()) {
			return $this->goHome();
		}
		
		return $this->render('reset', ['model' => $model]);
	}

	public function actionPassword($id = null) {
		$model = self::findModel($id ? $id : Yii::$app->user->id);

		$model->scenario = 'password';

		self::can('password', ['model' => $model]);

		unset($model->password);

		if ($post = Yii::$app->request->post()) {
			if ($model->load($post)) {
				$model->passworded_at = date('Y-m-d H:i:s');

				if ($model->save()) {
					return $this->redirect(['//ident/ident/view', 'id' => $model->id]);
				}
			}
		}

		return $this->render('password', ['model' => $model]);
	}

	public function actionLogout() {
		if ($adminId = Yii::$app->session->get('admin')) {
			$admin = (Yii::$app->user->identityClass)::findOne($adminId);

			Yii::$app->session->remove('admin');

			Yii::$app->user->login($admin);
		} else {
			Yii::$app->user->logout();
		}

		return $this->goHome();
	}

	public function actionProfile($lang_id = 'pl') {
		$model = Yii::$app->user->identity;

		self::can('profile', ['model' => $model]);

		if ($model->isInRole('identPerson')) {
			if (($post = Yii::$app->request->post()) && $model->load($post)) {
				$model->save();

				return $this->goHome();
			}

			return $this->render('profile', ['model' => $model, 'lang_id' => $lang_id]);
		}

		$this->goHome();
	}

	public static function findModel($id) {
		if ($model = (Yii::$app->user->identityClass)::findOne($id)) {
			return $model;
		} else {
			throw new NotFoundHttpException();
		}
	}
}
