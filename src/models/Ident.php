<?php

namespace datait\ident\models;

use Yii;
use common\components\ActiveRecord;

class Ident extends ActiveRecord implements \yii\web\IdentityInterface {
	const ACTIVE_NO = 0;
	const ACTIVE_YES = 1;

	public static function tableName() {
		return '{{%ident}}';
	}

	public function rules() {
		return array_merge(parent::rules(), [
			[['login', 'email'], 'email'],
			['login', 'unique'],
			['name', 'string', 'skipOnEmpty' => true],

			[['login', 'email'], 'required'],
		]);
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		return static::findOne(['access_token' => $token]);
	}

	public function getId() {
		return $this->id;
	}

	public function getAuthKey() {
		return $this->auth_key;
	}

	public function validateAuthKey($authKey) {
		return $this->auth_key === $authKey;
	}

	public function validatePass($pass) {
		return Yii::$app->security->validatePassword($pass, $this->pass);
	}

	public static function generatePass() {
		return substr(str_shuffle('abcdefghijkmnoprstuwz23456789!@#$%^&*()'), 0, 8);
	}

	public static function generateAuthKey() {
		return substr(str_shuffle('abcdefghijkmnoprstuwz23456789!@#$%^&*()'), 0, 32);
	}

	public function getTitle() {
		return Yii::t('ident', 'title') . ' #' . $this->id . ' ' . $this->name;
	}

	public function getUrl() {
		return ['ident/view', 'id' => $this->id];
	}

	public function createResetEmail() {
		//to do
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$this->pass = Yii::$app->security->generatePasswordHash($this->pass);

			return true;
		} else {
			return false;
		}
	}
}