<?php

declare(strict_types=1);

namespace tests;

use tests\data\EchoMigrateController;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\db\Exception;

/**
 * Class PermissionsDbTestCase
 * @package tests
 */
abstract class DbTestCase extends AppTestCase
{
    /**
     * @var Connection
     */
    protected static $db;

    /**
     * @var array [table => [row1 columns => values], [row2 columns => values], ...]
     */
    public $fixtures = [];

    /**
     * @var array
     */
    public static $params;

    /**
     * @var bool
     */
    public static $runMigrations = true;

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public static function getParam(string $name, $default = null)
    {
        if (static::$params === null) {
            static::$params = require __DIR__ . '/config.php';
        }
        return static::$params[$name] ?? $default;
    }

    /**
     * @throws Exception
     */
    public function fixturesUp(): void
    {
        foreach ($this->fixtures as $table => $data) {
            foreach ($data as $row) {
                static::$db->createCommand()->insert($table, $row)->execute();
            }
        }
    }

    /**
     * @throws Exception
     */
    public function fixturesDown(): void
    {
        static::$db->createCommand('SET FOREIGN_KEY_CHECKS=0;')->execute();
        foreach ($this->fixtures as $table => $data) {
            static::$db->createCommand()->truncateTable($table)->execute();
        }
        static::$db->createCommand('SET FOREIGN_KEY_CHECKS=1;')->execute();
    }

    /**
     * @return Connection
     * @throws Exception
     */
    public static function getConnection(): Connection
    {
        if (static::$db === null) {
            $dbConfig = static::getParam('mysql');

            $db = new Connection();
            $db->dsn = $dbConfig['dsn'];
            if (isset($dbConfig['username'])) {
                $db->username = $dbConfig['username'];
                $db->password = $dbConfig['password'];
            }
            if (isset($dbConfig['attributes'])) {
                $db->attributes = $dbConfig['attributes'];
            }
            if (!$db->isActive) {
                $db->open();
            }
            static::$db = $db;
        }
        return static::$db;
    }

    /**
     * @param string $route
     * @param array $params
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    protected static function runSilentMigration(string $route, array $params = []): void
    {
        ob_start();
        if (\Yii::$app->runAction($route, $params) === ExitCode::OK) {
            ob_end_clean();
        } else {
            fwrite(STDOUT, "\nMigration failed!\n");
            ob_end_flush();
        }
    }

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
    }

    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return [
            'components' => [
                'db' => static::getConnection(),
            ],
            'controllerMap' => [
                'migrate' => [
                    'class' => EchoMigrateController::class,
                    'migrationPath' => [
                        __DIR__ . '/data/migrations',
                        __DIR__ . '/../src/migrations',
                    ],
                    'interactive' => false
                ],
            ],
        ];
    }

    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        if (static::$runMigrations) {
            static::runSilentMigration('migrate/up');
        }
    }

    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public static function tearDownAfterClass(): void
    {
        if (static::$runMigrations) {
            static::runSilentMigration('migrate/down', ['all']);
        }
        if (static::$db) {
            static::$db->close();
        }
        parent::tearDownAfterClass();
    }
}
