<?php

declare(strict_types=1);

use yii\db\Migration;

/**
 * User test DB.
 *
 * Class m180808_094100_create_table_user
 * @package tests\data\migrations
 */
class m180808_094100_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'auth_key' => $this->string(45)->notNull()->defaultValue('auth_key'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('user');
    }
}
