<?php

declare(strict_types=1);

namespace tests\data;

use yii\console\controllers\MigrateController;
use yii\console\ExitCode;

/**
 * MigrateController that writes output via echo instead of using output stream. Allows us to buffer it.
 */
class EchoMigrateController extends MigrateController
{
    /**
     * {@inheritdoc}
     */
    public function stdout($string): void // BC declaration
    {
        echo $string;
    }

    /**
     * @param int $limit
     * @return int
     */
    public function actionUp($limit = 0): int // BC declaration
    {
        $result = parent::actionUp($limit);
        if ($result !== null) {
            return $result;
        }
        return ExitCode::OK;
    }

    /**
     * @param int $limit
     * @return int
     * @throws \yii\console\Exception
     */
    public function actionDown($limit = 1): int // BC declaration
    {
        $result = parent::actionDown($limit);
        if ($result !== null) {
            return $result;
        }
        return ExitCode::OK;
    }
}
