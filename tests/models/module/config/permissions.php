<?php

return [
    'userPermission' => [
        'children' => [
            'list',
            'create',
            'some',
            'next-one',
        ],
    ],
    'item' => [
        'children' => [
            'simple',
            'with-definition-and-rules' => [
                'definition' => [
                    'class' => \tests\models\module\permissions\WithDefinitionAndRulesPermissionDefinition::class,
                    'rules' => [
                        'UserIsVeryHandsomeRule' => \tests\models\module\rules\UserIsVeryHandsomeRule::class,
                        'OtherStupidRule' => [
                            'class' => \tests\models\module\rules\OtherStupidRule::class,
                            'params' => [
                                'isStupidRule',
                            ],
                        ],
                        'NextRule' => [
                            'class' => \tests\models\module\rules\NextRule::class,
                            'params' => [
                                'someParam' => 'someValue',
                                'otherParam' => null,
                            ],
                        ],
                    ],
                ],
                'children' => [
                    'on' => [
                        'children' => [
                            'next' => [
                                'children' => [
                                    'level' => [
                                        'definition' => \tests\models\module\permissions\ItemOnNextLevelPermissionDefinition::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'last',
        ],
    ],
];
