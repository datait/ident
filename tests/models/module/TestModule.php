<?php

declare(strict_types=1);

namespace tests\models\module;

use yii\base\Module;

/**
 * Class TestModule
 * @package tests\models\module
 */
class TestModule extends Module
{
    /**
     * @var array permissions configuration array for module
     */
    public $permissionConfig = [];

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();
        $this->permissionConfig = require __DIR__ . '/config/permissions.php';
    }
}
