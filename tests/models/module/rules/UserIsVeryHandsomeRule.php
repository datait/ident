<?php

declare(strict_types=1);

namespace tests\models\module\rules;

use keystone\permission\models\permission\Rule;
use keystone\permission\models\permission\RuleInterface;

/**
 * Class UserIsVeryHandsomeRule
 * @package tests\models\module\rules
 */
class UserIsVeryHandsomeRule extends Rule implements RuleInterface
{
    /**
     * @return bool returns true if email of current user name is 'Very Handsome'
     */
    public function check(): bool
    {
        return $this->user->name === 'Very Handsome';
    }
}
