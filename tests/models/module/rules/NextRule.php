<?php

declare(strict_types=1);

namespace tests\models\module\rules;

use keystone\permission\models\permission\Rule;
use keystone\permission\models\permission\RuleInterface;

/**
 * Class NextRule
 * @package tests\models\module\rules
 */
class NextRule extends Rule implements RuleInterface
{
    /**
     * @return bool returns true if $this->params['otherParam'] == $this->params['someParam']
     */
    public function check(): bool
    {
        return isset($this->params['someParam']) && isset($this->params['otherParam']) && $this->params['someParam'] == $this->params['otherParam'];
    }
}
