<?php

declare(strict_types=1);

namespace tests\models\module\rules;

use keystone\permission\models\permission\Rule;
use keystone\permission\models\permission\RuleInterface;

/**
 * Class OtherStupidRule
 * @package tests\models\module\rules
 */
class OtherStupidRule extends Rule implements RuleInterface
{
    /**
     * @return bool returns true if params $this->params['isStupidRule'] is true
     */
    public function check(): bool
    {
        return $this->params['isStupidRule'] ?? false;
    }
}
