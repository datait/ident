<?php

declare(strict_types=1);

namespace tests\models\module\permissions;

use keystone\permission\models\permission\PermissionDefinition;
use keystone\permission\models\permission\PermissionDefinitionInterface;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class WithDefinitionAndRulesPermissionDefinition
 * @package tests\models\module\permissions
 */
class WithDefinitionAndRulesPermissionDefinition extends PermissionDefinition implements PermissionDefinitionInterface
{
    /**
     * @return bool returns true if each rules returns true
     * @throws
     */
    public function check(): bool
    {
        $userIsVeryHandsome = $this->checkRule($this->rules['UserIsVeryHandsomeRule']);
        $otherStupidRule = $this->checkRule(\tests\models\module\rules\OtherStupidRule::class, [
            'isStupidRule' => true
        ]);

        $nextRule = Yii::createObject([
            'class' => $this->rules['NextRule']['class'],
            'params' => ArrayHelper::merge(
                $this->rules['NextRule']['params'],
                [
                    'otherParam' => $this->params['otherParam'],
                ]
            ),
        ])->check();

        return $userIsVeryHandsome && $otherStupidRule && $nextRule;
    }
}
