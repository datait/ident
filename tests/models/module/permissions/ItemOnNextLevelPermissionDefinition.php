<?php

declare(strict_types=1);

namespace tests\models\module\permissions;

use keystone\permission\models\permission\PermissionDefinition;

/**
 * Class ItemOnNextLevelPermissionDefinition
 * @package fronttests\modules\permission
 */
class ItemOnNextLevelPermissionDefinition extends PermissionDefinition
{
    /**
     * @return bool returns true current hour is between 8 and 16
     */
    public function check(): bool
    {
        return (int)date('H') >= 8 && (int)date('H') <= 15;
    }
}
