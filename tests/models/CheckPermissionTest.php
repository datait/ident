<?php
 
declare(strict_types=1);
 
namespace tests\models;
 
use tests\data\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\helpers\ArrayHelper;
 
/**
 * Class CheckPermissionTest
 * @package tests\models
 */
class CheckPermissionTest extends DbTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'Very Handsome',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Quasimodo',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     * @throws \yii\db\Exception
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
        Yii::$app->authManager->createModule('test');
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->authManager->deleteModule('test');
        Yii::$app->user->setIdentity(null);
    }
 
    public function testCheckSimplePermission(): void
    {
        $user1 = User::findOne(1);
        $user2 = User::findOne(2);

        Yii::$app->authManager->createUserPermission($user1->id, 'test.item');
        Yii::$app->user->setIdentity($user1);
        $this->assertTrue(Yii::$app->user->can('test.item'));
        Yii::$app->user->setIdentity(null);
        
        Yii::$app->user->setIdentity($user2);
        $this->assertFalse(Yii::$app->user->can('test.item'));
        Yii::$app->user->setIdentity(null);

        Yii::$app->authManager->deleteUserPermission($user1->id, 'test.item');
        Yii::$app->user->setIdentity($user1);
        $this->assertFalse(Yii::$app->user->can('test.item'));
        Yii::$app->user->setIdentity(null);
    }

    public function testCheckComplexPermission(): void
    {
        $user1 = User::findOne(1);
        $user2 = User::findOne(2);

        Yii::$app->authManager->createUserPermission($user1->id, 'test.item.with-definition-and-rules');
        Yii::$app->user->setIdentity($user1);
        $this->assertTrue(Yii::$app->user->can('test.item.with-definition-and-rules', ['otherParam' => 'someValue']));
        $this->assertFalse(Yii::$app->user->can('test.item.with-definition-and-rules', ['otherParam' => 'otherValue']));
        Yii::$app->user->setIdentity(null);

        Yii::$app->user->setIdentity($user2);
        $this->assertFalse(Yii::$app->user->can('test.item.with-definition-and-rules'));
        Yii::$app->user->setIdentity(null);

        Yii::$app->authManager->deleteUserPermission($user1->id, 'test.item.with-definition-and-rules');
        Yii::$app->user->setIdentity($user1);
        $this->assertFalse(Yii::$app->user->can('test.item.with-definition-and-rules'));
        Yii::$app->user->setIdentity(null);

        Yii::$app->authManager->createUserPermission($user1->id, 'test.item.with-definition-and-rules.on.next.level');
        Yii::$app->user->setIdentity($user1);
        $this->expectException(\yii\base\InvalidArgumentException::class);
        Yii::$app->user->can('test.item.with-definition-and-rules.on.next.level');
        Yii::$app->user->setIdentity(null);
    }
}
