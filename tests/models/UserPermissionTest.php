<?php
 
declare(strict_types=1);
 
namespace tests\modules\permission;
 
use keystone\account\enums\UserStatusEnum;
use keystone\account\models\user\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\helpers\ArrayHelper;
 
/**
 * Class UserPermissionTest
 * @package tests\permission
 */
class UserPermissionTest extends DbTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'With permission',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Without permission',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
        Yii::$app->authManager->createModule('test');
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->authManager->deleteModule('test');
        Yii::$app->user->setIdentity(null);
    }
 
    public function testCreateAndDeleteSimpleUserPermission(): void
    {
        $user1 = User::findOne(1);
        Yii::$app->authManager->createUserPermission($user1->id, 'test.item');
        $this->assertTrue(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item'));

        $user2 = User::findOne(2);
        $this->assertFalse(Yii::$app->authManager->existsUserPermission($user2->id, 'test.item'));

        Yii::$app->authManager->deleteUserPermission($user1->id, 'test.item');
        $this->assertFalse(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item'));
    }

    public function testCreateAndDeleteComplexUserPermission(): void
    {
        $user1 = User::findOne(1);
        Yii::$app->authManager->createUserPermission($user1->id, 'test.item.with-definition-and-rules');
        $this->assertTrue(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item.with-definition-and-rules'));

        $user2 = User::findOne(2);
        $this->assertFalse(Yii::$app->authManager->existsUserPermission($user2->id, 'test.item.with-definition-and-rules'));

        Yii::$app->authManager->deleteUserPermission($user1->id, 'test.item.with-definition-and-rules');
        $this->assertFalse(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item.with-definition-and-rules'));
    }
}
