<?php
 
declare(strict_types=1);
 
namespace tests\permission;
 
use tests\data\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\helpers\ArrayHelper;
 
/**
 * Class PermissionTest
 * @package tests\permission
 */
class PermissionTest extends DbTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'Very Handsome',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Quasimodo',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     * @throws \yii\db\Exception
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->user->setIdentity(null);
    }

    public function testCreateModule()
    {
        Yii::$app->authManager->createModule('test');
        $this->assertTrue(Yii::$app->authManager->existsPermission('test.item.simple'));
        $this->assertTrue(Yii::$app->authManager->getPermission('test.item.simple')->parent_id === 'test.item');
        $this->assertTrue(Yii::$app->authManager->getPermission('test.item.simple')->parent->parent_id === 'test');
        $this->assertNull(Yii::$app->authManager->getPermission('test.item.simple')->parent->parent->parent_id);

        $this->assertTrue(Yii::$app->authManager->getPermission('test.item.simple')->module_id === 'test');

        $user1 = User::findOne(1);
        Yii::$app->authManager->createUserPermission($user1->id, 'test.item');
        $this->assertTrue(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item'));
    }
 
    public function testDeleteModule()
    {
        Yii::$app->authManager->deleteModule('test');
        $this->assertFalse(Yii::$app->authManager->existsPermission('test.item.simple'));

        $user1 = User::findOne(1);
        $this->assertFalse(Yii::$app->authManager->existsUserPermission($user1->id, 'test.item'));
    }
}
