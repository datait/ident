<?php
 
declare(strict_types=1);
 
namespace tests\permission;

use keystone\account\models\user\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\helpers\ArrayHelper;
 
/**
 * Class SyncTest
 * @package tests\permission
 */
class SyncTest extends DbTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'Very Handsome',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Quasimodo',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
        Yii::$app->authManager->createModule('test');
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->authManager->deleteModule('test');
        Yii::$app->user->setIdentity(null);
    }
 
    public function testToRemoveModulePermission(): void
    {
        Yii::$app->authManager->createPermission([
            'id' => 'test.item.new',
            'parent_id' => 'test.item',
            'module_id' => 'test',
        ]);
        $this->assertTrue(Yii::$app->authManager->existsPermission('test.item.new'));

        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertTrue(isset($diff['toRemove'][0]) && $diff['toRemove'][0] === 'test.item.new');

        Yii::$app->authManager->syncModule('test');
        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertSame($diff, ['toAdd' => [], 'toRemove' => []]);
    }

    public function testToAddModulePermission(): void
    {
        Yii::$app->authManager->deletePermission('test.item.last');
        
        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertTrue(isset($diff['toAdd'][0]['id']) && $diff['toAdd'][0]['id'] === 'test.item.last');

        Yii::$app->authManager->syncModule('test');
        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertSame($diff, ['toAdd' => [], 'toRemove' => []]);
    }

    public function testToRemoveApplicationPermission(): void
    {
        Yii::$app->authManager->createPermission([
            'id' => 'test.item.new',
            'parent_id' => 'test.item',
            'module_id' => 'test',
        ]);
        $this->assertTrue(Yii::$app->authManager->existsPermission('test.item.new'));

        $diff = Yii::$app->authManager->diffApp();
        $this->assertTrue(isset($diff['toRemove'][0]) && $diff['toRemove'][0] == 'test.item.new');

        Yii::$app->authManager->syncApp();
        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertSame($diff, ['toAdd' => [], 'toRemove' => []]);
    }

    public function testToAddApplicationPermission(): void
    {
        Yii::$app->authManager->deletePermission('test.item.last');
        
        $diff = Yii::$app->authManager->diffApp();
        $this->assertTrue(isset($diff['toAdd'][0]['id']) && $diff['toAdd'][0]['id'] === 'test.item.last');

        Yii::$app->authManager->syncApp();
        $diff = Yii::$app->authManager->diffModule('test');
        $this->assertSame($diff, ['toAdd' => [], 'toRemove' => []]);
    }
}
