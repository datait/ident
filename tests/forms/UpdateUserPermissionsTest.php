<?php

declare(strict_types=1);

namespace tests\forms;

use keystone\permission\forms\UpdateUserPermissionsForm;
use tests\data\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class RegisterAccountTest
 * @package tests\forms
 */
class UpdateUserPermissionsTest extends DbTestCase
{
    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'Very Handsome',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Quasimodo',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
                'i18n' => [
                    'translations' => [
                        'permission.*' => [
                            'class' => \yii\i18n\PhpMessageSource::class,
                            'sourceLanguage' => 'en',
                            'forceTranslation' => true,
                            'basePath' => '@app/messages',
                        ],
                    ],
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
        Yii::$app->authManager->createModule('test');
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->authManager->deleteModule('test');
        Yii::$app->user->setIdentity(null);
    }

    public function testPermissionNotExists(): void
    {
        $user = User::findOne(1);
        $model = new UpdateUserPermissionsForm([
            'userId' => $user->id,
            'permissions' => [
                'non.existing.permission' => 1,
            ],
        ]);

        $this->assertFalse($model->validate());
        $this->assertEquals(["There is no such permission like 'non.existing.permission'"], $model->errors['permissions']);
    }

    public function testUserNotExists(): void
    {
        $model = new UpdateUserPermissionsForm([
            'userId' => 3,
            'permissions' => [
                'test' => 1,
            ],
        ]);

        $this->assertFalse($model->validate());
        $this->assertEquals(["User Id is invalid."], $model->errors['userId']);
    }

    
}
