<?php

declare(strict_types=1);

namespace tests\events;

use keystone\permission\forms\UpdateUserPermissionsForm;
use tests\data\User;
use keystone\permission\models\PermissionHandler;
use tests\DbTestCase;
use tests\models\module\TestModule;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;

/**
 * Class EventTest
 * @package tests\events
 */
class EventTest extends DbTestCase
{
    /**
     * @var array
     */
    protected static $eventsRaised = [];

    /**
     * @var array
     */
    public $fixtures = [
        'user' => [
            [
                'id' => 1,
                'name' => 'Very Handsome',
                'auth_key' => 'ccc',
            ],
            [
                'id' => 2,
                'name' => 'Quasimodo',
                'auth_key' => 'bbb',
            ],
        ],
    ];

    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return ArrayHelper::merge(parent::additionalConfig(), [
            'components' => [
                'user' => [
                    'identityClass' => User::class,
                ],
                'authManager' => [
                    'class' => PermissionHandler::class,
                    'identityClass' => User::class,
                    'cache' => 'cache',
                ],
                'i18n' => [
                    'translations' => [
                        'permission.*' => [
                            'class' => \yii\i18n\PhpMessageSource::class,
                            'sourceLanguage' => 'en',
                            'forceTranslation' => true,
                            'basePath' => '@app/messages',
                        ],
                    ],
                ],
            ],
            'modules' => [
                'test' => [
                    'class' => TestModule::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->fixturesUp();
        Yii::$app->authManager->createModule('test');
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        $this->fixturesDown();
        Yii::$app->authManager->deleteModule('test');
        Yii::$app->user->setIdentity(null);
    }

    public function testCheckUserPermission(): void
    {
        Event::on(PermissionHandler::class, PermissionHandler::EVENT_BEFORE_CHECK, function ($event) {
            static::$eventsRaised[PermissionHandler::EVENT_BEFORE_CHECK] = true;
        });
        Event::on(PermissionHandler::class, PermissionHandler::EVENT_AFTER_CHECK, function ($event) {
            static::$eventsRaised[PermissionHandler::EVENT_AFTER_CHECK] = true;
        });

        $user = User::findOne(1);

        Yii::$app->authManager->createUserPermission($user->id, 'test.item');
        Yii::$app->user->setIdentity($user);
        $this->assertTrue(Yii::$app->user->can('test.item'));
        Yii::$app->user->setIdentity(null);

        $this->assertArrayHasKey(PermissionHandler::EVENT_BEFORE_CHECK, static::$eventsRaised);
        $this->assertArrayHasKey(PermissionHandler::EVENT_AFTER_CHECK, static::$eventsRaised);
    }

    public function testUpdateUserPermissions(): void
    {
        Event::on(UpdateUserPermissionsForm::class, UpdateUserPermissionsForm::EVENT_BEFORE_UPDATE, function ($event) {
            static::$eventsRaised[UpdateUserPermissionsForm::EVENT_BEFORE_UPDATE] = true;
        });
        Event::on(UpdateUserPermissionsForm::class, UpdateUserPermissionsForm::EVENT_AFTER_UPDATE, function ($event) {
            static::$eventsRaised[UpdateUserPermissionsForm::EVENT_AFTER_UPDATE] = true;
        });

        $user = User::findOne(1);

        $model = new UpdateUserPermissionsForm([
            'userId' => $user->id,
            'permissions' => [
                'test.item' => 1,
            ],
        ]);
        $this->assertTrue($model->validate());

        $model->update(false);

        $this->assertArrayHasKey(UpdateUserPermissionsForm::EVENT_BEFORE_UPDATE, static::$eventsRaised);
        $this->assertArrayHasKey(UpdateUserPermissionsForm::EVENT_AFTER_UPDATE, static::$eventsRaised);
    }
}
