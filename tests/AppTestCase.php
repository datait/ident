<?php

declare(strict_types=1);

namespace tests;

/**
 * Class PermissionsAppTestCase
 * @package tests
 */
abstract class AppTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @return array additional mocked app config
     */
    public static function additionalConfig(): array
    {
        return [];
    }

    public static function setUpBeforeClass(): void
    {
        static::mockApplication();
    }

    public static function tearDownAfterClass(): void
    {
        \Yii::$app = null;
    }

    /**
     * @param array $config
     * @param string $appClass
     */
    protected static function mockApplication(array $config = [], string $appClass = \yii\web\Application::class): void
    {
        new $appClass(\yii\helpers\ArrayHelper::merge(
            [
                'id' => 'keystone-permisssions-test',
                'aliases' => [
                    '@bower' => '@vendor/bower-asset',
                    '@npm' => '@vendor/npm-asset',
                ],
                'basePath' => __DIR__ . '/../src',
                'runtimePath' => __DIR__ . '/runtime',
                'vendorPath' => __DIR__ . '/../vendor',
                'bootstrap' => [],
                'components' => [
                    'assetManager' => [
                        'basePath' => __DIR__ . '/runtime/assets',
                    ],
                    'urlManager' => [
                        'showScriptName' => true,
                    ],
                    'request' => [
                        'enableCookieValidation' => false,
                        'scriptFile' => __DIR__ . '/index.php',
                        'scriptUrl' => '/index.php',
                    ],
                    'authManager' => [
                        'class' => \keystone\permission\models\PermissionHandler::class,
                    ],
                ],
            ],
            static::additionalConfig(),
            $config
        ));
    }
}
